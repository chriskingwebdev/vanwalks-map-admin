import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet, View, Text, TouchableWithoutFeedback } from 'react-native';
import MapView, { Polyline } from "react-native-maps";
import * as Location from 'expo-location';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Ionicons } from '@expo/vector-icons';

import { colors, mapStyle, footer, buttonStyles, resetToUser } from "./styles.js"
import UserPosition from "./components/UserPosition";
import RoutePoint from "./components/RoutePoint";
import Header from "./components/Header";
import CustomMarker from "./components/CustomMarker"


const storeData = async (key, value) => {
    try {
        const jsonValue = JSON.stringify(value)
        await AsyncStorage.setItem(key, jsonValue)
    } catch (e) {
        // saving error
    }
}

const getData = async (key) => {
    try {
        const jsonValue = await AsyncStorage.getItem(key)
        return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch(e) {
        // error reading value
    }
}
  

export default function App() {
    const initialRegion = {
        longitudeDelta: 0.006740391254410838,
        latitude: 49.28549821695583,
        longitude: -123.11209760606289,
        latitudeDelta: 0.007820180004920019,
    };

    const mapRef = useRef();

    const [userPosition, setUserPosition] = useState(false);
    const [regions, setRegions] = useState([]);
    const [points, setPoints] = useState([]);
    const [markers, setMarkers] = useState([]);

    const [selectedType, setSelectedType] = useState(null);
    const [createMode, setCreateMode] = useState(null);

    const clearData = (key) => {
        switch(key) {
            case "points":
                setPoints([]);
                storeData("points", []);
                break;
            case "markers": 
                setMarkers([]);
                storeData("markers", []);
                break;
            case "regions":
                setRegions({})
                storeData("regions", []);
                break;
        }
    }

    const updatePoint = (newPoint, index) => {
        const newPoints = points.map((point, i) => {
            return i === index ? newPoint : point
        })
        setPoints(newPoints);
        storeData("points", newPoints);
    }

    const deletePoint = (index) => {
        const filteredPoints = points.filter((point, i) => index !== i) || [];
        setPoints(filteredPoints);
        storeData("points", filteredPoints);
    }

    const splitPoint = (index) => {
        const first = points.slice(0, index + 1);
        // const new = {latitude: points[index].latitude + 0.0000001, longitude: }
        const second = points.slice(index);
        const newPoints = [...first, ...second];
        setPoints(newPoints);
        storeData("points", newPoints);
    }

    const createItem = (type, text, positionType) => {

        let { latitude, longitude } = userPosition;
        if (positionType == "map-center") {
            const region = mapRef.current.__lastRegion;
            latitude = region.latitude;
            longitude = region.longitude;
        }

        if (type == "markers") {
            const newMarkers = [...markers, {latitude, longitude, text}];
            setMarkers(newMarkers);
            storeData("markers", newMarkers);
        } else if (type == "regions") {
            const newRegions = [...regions, {...mapRef.current.__lastRegion, text}];
            setRegions(newRegions);
            storeData("regions", newRegions);
        } else if (type == "points") {
            const newPoints = [...points, {latitude, longitude}];
            setPoints(newPoints);
            storeData("points", newPoints);
        }
    }

    const importItems = (type, items) => {
        if (type == "markers") {
            const newMarkers = [...markers, ...items];
            setMarkers(newMarkers);
            storeData("markers", newMarkers);
        } else if (type == "regions") {
            const newRegions = [...regions, ...items];
            setRegions(newRegions);
            storeData("regions", newRegions);
        } else if (type == "points") {
            const newPoints = [...points, ...items];
            setPoints(newPoints);
            storeData("points", newPoints);
        }
    }

    const updateMarker = (newMarker, index) => {
        const newMarkers = markers.map((marker, i) => {
            return i === index ? newMarker : marker
        })
        setMarkers(newMarkers);
        storeData("markers", newMarkers);
    }

    const deleteMarker = (index) => {
        const filteredMarkers = markers.filter((marker, i) => index !== i) || [];
        setMarkers(filteredMarkers);
        storeData("markers", filteredMarkers);
    }

    const deleteRegion = (index) => {
        const filteredRegions = regions.filter((region, i) => index !== i) || [];
        setRegions(filteredRegions);
        storeData("regions", filteredRegions);
    }

    const goToRegion = (index) => {
        mapRef.current.animateToRegion(regions[index], 500);
    }

    const centerMapOnUser = () => {
        mapRef.current.animateToRegion(userPosition, 500);
    }

    const userGetCurrentPosition = async () => {
        const res = await Location.getCurrentPositionAsync({
            accuracy: Location.Accuracy.BestForNavigation
        });
        setUserPosition(res.coords);
        const newRegion = { ...initialRegion, ...res.coords };
        mapRef.current.animateToRegion(newRegion, 500);
    }

    const enableLocationWatcher = () => {
        Location.watchPositionAsync({
            accuracy: Location.Accuracy.BestForNavigation,
            timeInterval: 1000,
            distanceInterval: 1
        }, (position) => setUserPosition(position.coords));
    }

    useEffect(() => {
        const askForPermissions = async () => {
            let { status } = await Location.requestForegroundPermissionsAsync();

            const locationPermissionGranted = status === 'granted';
            if(locationPermissionGranted) {
                userGetCurrentPosition();
                enableLocationWatcher();
            }
        }
        const loadStoredData = async () => {
            const points = await getData("points");
            points && setPoints(points);

            const markers = await getData("markers");
            markers && setMarkers(markers);

            const regions = await getData("regions");
            regions && setRegions(regions);

        }
        askForPermissions();
        loadStoredData();
    }, []);
    
    return (
        <View style={styles.container}>
        	<MapView
                ref={mapRef}
                style={{
                    flex: 1,
                    width: "100%"
                }}
                provider={MapView.PROVIDER_GOOGLE}
                initialRegion={initialRegion}
                customMapStyle={mapStyle}
                showsBuildings={false}
                showsPointsOfInterest={false}
                showsTraffic={false}
                showsIndoors={false}
                showsIndoorLevelPicker={false}
            >
                {
                    userPosition && <UserPosition
                        userPosition={userPosition}
                    />
                }
                {
                    points.length > 0 && <Polyline
                        coordinates={points}
                        strokeWidth={4}
                        strokeColor={colors.actionRed}
                    />
                }
                {
                    selectedType == "points" && points.map((point, index) => {
                        return <RoutePoint
                            key={index}
                            point={point}
                            index={index}
                            updatePoint={updatePoint}
                        />
                    })
                }
                {
                    markers.map((marker, index) => {
                        return <CustomMarker
                            key={index}
                            marker={marker}
                            index={index}
                            updateMarker={updateMarker}
                        />
                    })
                }
            </MapView>
            <Header
                points={points}
                markers={markers}
                regions={regions}
                clearData={clearData}
                selectedType={selectedType}
                setSelectedType={setSelectedType}
                createMode={createMode}
                setCreateMode={setCreateMode}
                createItem={createItem}
                importItems={importItems}
                deleteMarker={deleteMarker}
                deletePoint={deletePoint}
                splitPoint={splitPoint}
                deleteRegion={deleteRegion}
                goToRegion={goToRegion}
            />
            <TouchableWithoutFeedback key="reset-to-user" onPress={() => {
                centerMapOnUser();
            }}>
                <View style={resetToUser}>
                    <Ionicons name="ios-walk" size={24} style={{
                        color: "#FFF"
                    }} />
                </View>
        
            </TouchableWithoutFeedback>
            <View style={footer}>
                <TouchableWithoutFeedback onPress={() => {
                    setCreateMode("points");
                }}>
                    <View style={buttonStyles}>
                        <Text>Add Point</Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => {
                    setCreateMode("markers");
                }}>
                    <View style={{
                        ...buttonStyles,
                        backgroundColor: colors.highlightYellow
                    }}>
                        <Text>Add Marker</Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => {
                    setCreateMode("regions");
                }}>
                    <View style={{
                        ...buttonStyles,
                        backgroundColor: colors.userBlue
                    }}>
                        <Text>Add Region</Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

import React, { useState, useEffect }  from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Marker } from "react-native-maps";

import { colors } from "../styles.js"

const styles = StyleSheet.create({
    position: {
        width: 20,
        height: 20,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: colors.actionRed,
        backgroundColor: "#ffffff",
        justifyContent: "center",
        alignItems: "center",
    },
    text: {
        fontSize: 9
    }
});

const RoutePoint = props => {

    const [tracksViewChanges, setTracksViewChanges] = useState(false);
    let isSubscribed = true;

    const setFalse = () => {
        setTimeout(() => {
            isSubscribed && setTracksViewChanges(false);
        }, 1);
    };

    useEffect(() => {
        setTracksViewChanges(true);
        setFalse();
        isSubscribed = true;
        return () => {
            isSubscribed = false;
        }
    }, [props.point])


    const [isDragging, setIsDragging] = useState(false);

    const { point, index, updatePoint } = props;

    return <Marker
        coordinate={point}
        anchor={{x: 0.5, y: 0.5}}
        zIndex={index}
        draggable={true}
        tracksViewChanges={tracksViewChanges}
        onDragStart={() => setIsDragging(true)}
        onDragEnd={(e) => {
            setIsDragging(false);
            updatePoint(e.nativeEvent.coordinate, index);
        }}
    >
        <View style={{
            ...styles.position,
            ...(isDragging && { backgroundColor: colors.actionRed})
        }}>
            <Text style={styles.text}>{index}</Text>
        </View>
    </Marker>
};
export default RoutePoint
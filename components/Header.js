import React, { useState }  from 'react';
import { View, Text, TextInput, TouchableWithoutFeedback, StyleSheet, ScrollView, Clipboard } from 'react-native';

import { colors } from "../styles.js"

const styles = StyleSheet.create({
    header: {
        position: "absolute",
        top: 0,
        width: "100%",
        padding: 10,
        paddingTop: 50,
        backgroundColor: "white"
    },
    headerTop: {
        flexDirection: "row",
        justifyContent: "space-around",
        marginBottom: 10,
    },
    headerItem: {
        borderWidth: 2,
        borderColor: colors.lightGreen,
        borderRadius: 5,
        paddingHorizontal: 8,
        paddingVertical: 3
    },
    headerContent: {

    },
    contentText: {
    },
    contentFooter: {
        flexDirection: "row",
        justifyContent: "space-around",
        marginTop: 8
    },
    contentFooterItem: {
        borderWidth: 2,
        borderColor: colors.darkGreen,
        borderRadius: 5,
        paddingHorizontal: 8,
        paddingVertical: 3
    },
    textBox: { 
        marginTop: 5,
        height: 30, 
        borderWidth: 2,
        borderColor: colors.darkGreen,
        borderRadius: 5,
    },
    list: {
        maxHeight: 125
    },
    item: {
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 4,
        borderBottomWidth: 1,
        borderBottomColor: "#ddd"
    },
    itemText: {
        width: "auto",
        // flex: 1,
        fontSize: 14
    },
    itemButton: {
        marginLeft: "auto",
        color: colors.actionRed,
        fontSize: 14,
        borderWidth: 1,
        borderColor: colors.actionRed,
        paddingHorizontal: 5,
        paddingVertical: 2,
        borderRadius: 2,
    },

});

const Header = props => {

    const { points, markers, regions, clearData, createMode, setCreateMode, createItem, importItems, deleteMarker, deletePoint, splitPoint, goToRegion, deleteRegion, selectedType, setSelectedType } = props;

    const [newItemName, setNewItemName] = useState(null);
    const [importMode, setImportMode] = useState(false);
    const [importData, setImportData] = useState("");
    const [importError, setImportError] = useState(null);
    const [showConfirm, setShowConfirm] = useState(false);

    const toggleSelectedType = (type) => {
        const newType = type == selectedType ? null : type;
        setSelectedType(newType);
    }

    let headerContent = <></>;

    if (importMode) {
        headerContent = <View style={headerContent}>
            <Text> Import { `${selectedType.charAt(0).toUpperCase()}${selectedType.slice(1)}` }:</Text>
            <TextInput
                multiline
                numberOfLines={4}
                style={{
                    ...styles.textBox,
                    height: 100
                }}
                onChangeText={text => setImportData(text)}
                value={importData}
            />
            { importError && <Text>{ importError }</Text> }
            <View style={styles.contentFooter}>
                <TouchableWithoutFeedback onPress={() => {
                    setImportMode(null);
                    setImportData("");
                    setImportError(null);
                }}>
                    <View style={styles.contentFooterItem}>
                        <Text>
                            Cancel
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => {
                    try {
                        const newItems = JSON.parse(importData);
                        importItems(selectedType, newItems);
                        setImportMode(null);
                        setImportData("");
                        setImportError(null);
                    } catch (err) {
                        setImportError("There was an error importing the data");
                    }
                }}>
                    <View style={styles.contentFooterItem}>
                        <Text>
                            Import
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        </View>
    } else if (createMode) {
        headerContent = <View style={headerContent}>
            { createMode !== "points" && <><Text>{ `${createMode.charAt(0).toUpperCase()}${createMode.slice(1,createMode.length - 1)}` } Name:</Text>
                <TextInput
                    style={styles.textBox}
                    onChangeText={text => setNewItemName(text)}
                    value={newItemName}
                />
            </> }
            <View style={styles.contentFooter}>
                <TouchableWithoutFeedback onPress={() => {setCreateMode(null)}}>
                    <View style={styles.contentFooterItem}>
                        <Text>
                            Cancel
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => {
                    createItem(createMode, newItemName, "user");
                    setCreateMode(null);
                    setNewItemName("");
                }}>
                    <View style={styles.contentFooterItem}>
                        <Text>
                            Create at User
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => {
                    createItem(createMode, newItemName, "map-center");
                    setCreateMode(null);
                    setNewItemName("");
                }}>
                    <View style={styles.contentFooterItem}>
                        <Text>
                            Create at Map Center
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        </View>
    } else if (selectedType) {
        let content = "";
        switch(selectedType) {
            case "points":
                content = <ScrollView style={styles.list}>
                    {points.map((point, index) => {
                        return <View key={index} style={styles.item}>
                            <Text style={styles.itemText}>{index}</Text>
                            {/* <Text style={styles.itemText}>{JSON.stringify(point)}</Text> */}
                            <TouchableWithoutFeedback onPress={() => {splitPoint(index)}}>
                                <Text style={styles.itemButton}>Split</Text>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback onPress={() => {deletePoint(index)}}>
                                <Text style={styles.itemButton}>X</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    })}
                </ScrollView>;
                break;
            case "markers":
                content = <ScrollView style={styles.list}>
                    {markers.map((marker, index) => {
                        return <View key={index} style={styles.item}>
                            <Text style={styles.itemText}>{marker.text}</Text>
                            <TouchableWithoutFeedback onPress={() => {deleteMarker(index)}}>
                                <Text style={styles.itemButton}>X</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    })}
                </ScrollView>
                break;
            case "regions":
                content = <ScrollView style={styles.list}>
                    {regions.map((region, index) => {
                        return <View key={index} style={styles.item}>
                            <Text style={styles.itemText}>{region.text}</Text>
                            <TouchableWithoutFeedback onPress={() => {goToRegion(index)}}>
                                <Text style={styles.itemButton}>Go To</Text>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback onPress={() => {deleteRegion(index)}}>
                                <Text style={styles.itemButton}>X</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    })}
                </ScrollView>
                break;
        }

        const buttons = showConfirm ? <>
            <Text>Delete all {selectedType}?</Text>
            <TouchableWithoutFeedback onPress={() => {
                clearData(selectedType);
                setShowConfirm(false);
            }}>
                <View style={styles.contentFooterItem}>
                    <Text>
                        Yes
                    </Text>
                </View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={() => {setShowConfirm(false)}}>
                <View style={styles.contentFooterItem}>
                    <Text>
                        No
                    </Text>
                </View>
            </TouchableWithoutFeedback>
        </> : <>
            <TouchableWithoutFeedback onPress={() => {setSelectedType(null)}}>
                <View style={styles.contentFooterItem}>
                    <Text>
                        Close
                    </Text>
                </View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={() => {setShowConfirm(true)}}>
                <View style={styles.contentFooterItem}>
                    <Text>
                        Clear
                    </Text>
                </View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={() => {setImportMode(true)}}>
                <View style={styles.contentFooterItem}>
                    <Text>
                        Import
                    </Text>
                </View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={() => {
                try {
                    const data = props[selectedType];
                    Clipboard.setString(JSON.stringify(data));
                } catch (err) {
                    setImportError("There was an error copying the data");
                }
            }}>
                <View style={styles.contentFooterItem}>
                    <Text>
                            Copy
                    </Text>
                </View>
            </TouchableWithoutFeedback>
        </>

        headerContent = <View style={headerContent}>
            {content}
            <View style={styles.contentFooter}>
                {buttons}
            </View>
        </View>
    }

    return <View style={styles.header}>
        <View style={styles.headerTop}>
            <TouchableWithoutFeedback onPress={() => toggleSelectedType("points")}>
                <View style={{
                    ...styles.headerItem,
                    ...(selectedType == "points" && { backgroundColor: colors.lightGreen })
                }}>
                    <Text>
                        Points
                    </Text>
                </View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={() => toggleSelectedType("markers")}>
                <View style={{
                    ...styles.headerItem,
                    ...(selectedType == "markers" && { backgroundColor: colors.lightGreen })
                }}>
                    <Text>
                        Markers
                    </Text>
                </View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={() => toggleSelectedType("regions")}>
                <View style={{
                    ...styles.headerItem,
                    ...(selectedType == "regions" && { backgroundColor: colors.lightGreen })
                }}>
                    <Text>
                        Regions
                    </Text>
                </View>
            </TouchableWithoutFeedback>
        </View>
        {
            headerContent
        }
    </View>
};
export default Header
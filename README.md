# VanWalks - Map Admin

The React Native App to build routes for VanWalks

### DEBUG LOCALLY
`yarn start`
Navigate to `http://localhost:19002/` in a browser
Click the `Run on iOS simulator` to open a local simulator. (Xcode must be installed)
You can also open the project on the Expo iOS app by using the QR code

### TO CHANGE LOCATION IN THE SIMULATOR

https://github.com/lyft/set-simulator-location

`brew install lyft/formulae/set-simulator-location`

Demo Path 1 - Near Victory Square
`set-simulator-location -c 49.282469 -123.109823 #demo-1`

Demo Path 2
`set-simulator-location -c 49.283901 -123.111961 #demo-2`

Demo Path 3
`set-simulator-location -c 49.284565 -123.110791 #demo-3`

Demo Path 4
`set-simulator-location -c 49.284261 -123.109091 #demo-4`
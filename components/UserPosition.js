import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Marker } from "react-native-maps";

const userPositionStyles = StyleSheet.create({
    position: {
        width: 20,
        height: 20,
        borderRadius: 10,
        borderWidth: 2,
        borderColor: "#EEFCFF",
        backgroundColor: "#30C0D3",
        justifyContent: "center",
        alignItems: "center"
    }
});

const UserPosition = props => {

    const { userPosition } = props;

    if (!userPosition) return null;

    return <Marker
        coordinate={userPosition}
        anchor={{x: 0.5, y: 0.5}}
    >
        <View style={userPositionStyles.position}>
        </View>
    </Marker>
};
export default UserPosition
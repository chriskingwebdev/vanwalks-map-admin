import React, { useState }  from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Marker } from "react-native-maps";
import { FontAwesome5 } from '@expo/vector-icons';

import { colors } from "../styles.js"

const markerHeight = 36;
let textPaddingInside = 20;
let textPaddingOutside = 10;


const styles = StyleSheet.create({
    position: {
        justifyContent: "center",
        alignItems: "center"
    },
    flyoutContainer: {
        height: markerHeight-1
    },
    textContainer: {
        paddingTop: 2,
        paddingBottom: 2,
        backgroundColor: "#ffffff",
        paddingLeft: textPaddingInside,
        paddingRight: textPaddingOutside,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10
        // ...(isLeft ? {
        //     paddingLeft: textPaddingOutside,
        //     paddingRight: textPaddingInside,
        //     borderTopLeftRadius: 10,
        //     borderBottomLeftRadius: 10
        // } : {
        //     paddingLeft: textPaddingInside,
        //     paddingRight: textPaddingOutside,
        //     borderTopRightRadius: 10,
        //     borderBottomRightRadius: 10
        // })
    },
});

const RoutePoint = props => {

    const [isDragging, setIsDragging] = useState(false);

    const { marker, index, updateMarker } = props;

    return <>
        <Marker
            coordinate={marker}
            anchor={{x: 0.5, y: 1}}
            zIndex={2}
            draggable={true}
            onDragStart={() => setIsDragging(true)}
            onDragEnd={(e) => {
                console.log(e.nativeEvent.coordinate)
                setIsDragging(false);
                updateMarker({
                    ...e.nativeEvent.coordinate,
                    text: marker.text
                }, index);
            }}
        >
            <View style={styles.position}>
                <FontAwesome5 name="map-marker" size={36}  style={{
                    color: isDragging ? colors.actionRed : colors.highlightYellow
                }} />
            </View>
        </Marker>
        { !isDragging && <Marker
            coordinate={marker}
            anchor={{x: 0, y: 1}}
            zIndex={1}
        ><View style={styles.flyoutContainer}>
                <View style={styles.textContainer}>
                    <Text style={styles.header}>{ marker.text }</Text>
                </View>
            </View>
        </Marker>}
    </>;
};
export default RoutePoint
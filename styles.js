export const colors = {
    actionRed: "#DF0505",
    userBlue: "#30C0D3",
    lightGreen: "#b3dbcf",
    darkGreen: "#287773",
    highlightYellow: "#f2b73c",
    actionGreen: "#1BC860",
}

const styles = {
    geometry: "#131c2f",
    labelFill: "#000000",
    labelStroke: "#ffffff",
    land: colors.darkGreen,
    road: "#DDDDDD",
    water: colors.lightGreen,
};

export const mapStyle = [
    {
        elementType: "geometry",
        stylers: [
            {
                color: styles.geometry,
            },
        ],
    },
    {
        elementType: "labels.text.fill",
        stylers: [
            {
                color: styles.labelFill,
            },
        ],
    },
    {
        elementType: "labels.text.stroke",
        stylers: [
            {
                color: styles.labelStroke,
            },
        ],
    },
    {
        featureType: "landscape.natural",
        elementType: "geometry",
        stylers: [
            {
                color: styles.land,
            },
        ],
    },
    {
        featureType: "road",
        elementType: "geometry",
        stylers: [
            {
                color: styles.road,
            },
        ],
    },
    {
        featureType: "water",
        elementType: "geometry.fill",
        stylers: [
            {
                color: styles.water,
            },
        ],
    },
    {
        featureType: "water",
        elementType: "labels.text.fill",
        stylers: [
            {
                color: styles.water,
            },
        ],
    },
    {
        featureType: "water",
        elementType: "labels.text.stroke",
        stylers: [
            {
                color: styles.labelStroke,
            },
        ],
    },

    // just things that are turned off
    {
        featureType: "administrative",
        elementType: "labels",
        stylers: [
            {
                "visibility": "off"
            }
        ]
    },
    {
        featureType: "administrative",
        elementType: "geometry",
        stylers: [
            {
                visibility: "off",
            },
        ],
    },
    {
        featureType: "landscape.man_made",
        elementType: "labels",
        stylers: [
            {
                "visibility": "off"
            }
        ]
    },
    {
        featureType: "poi",
        stylers: [
            {
                visibility: "off",
            },
        ],
    },
    {
        featureType: "road",
        elementType: "labels.icon",
        stylers: [
            {
                visibility: "off",
            },
        ],
    },
    {
        "featureType": "transit",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
];

export const footer = {
    position: "absolute",
    bottom: 0,
    paddingTop: 15,
    paddingBottom: 20,
    flexDirection: "row",
    justifyContent: "space-evenly",
    backgroundColor: "#fff",
    width: "100%"
}

export const buttonStyles = {
    backgroundColor: colors.actionRed,
    // width: 35,
    // height: 35,
    borderRadius: 6,
    marginHorizontal: 20,
    paddingHorizontal: 10,
    paddingVertical: 5,
    
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
}

export const resetToUser = {

    position: "absolute",
    right: 10,
    bottom: 80,

    borderRadius: 24,
    width: 48,
    height: 48,
    marginHorizontal: 10,

    backgroundColor: colors.actionGreen,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10
};